import EventBus from '../eventBus'
const Datastore = window.require('nestdb')
const Path = window.require('path')
const { remote } = window.require('electron')

const db = {}
db.clientes = new Datastore({
  filename: Path.join(remote.app.getAppPath(), 'database', 'clientes.db'),
  timestampData: true,
  autoload: true
})
db.receitas = new Datastore({
  filename: Path.join(remote.app.getAppPath(), 'database', 'receitas.db'),
  timestampData: true,
  autoload: true
})

db.clientes.on('inserted', (cliente) => {
  db.clientes.find({}, (error, clientes) => EventBus.$emit('INSERT_CLIENTE_SUCCESS', cliente, clientes))
})

db.clientes.on('updated', (newCliente, oldCliente) => {
  EventBus.$emit('UPDATE_CLIENTE_SUCCESS', newCliente, oldCliente)
})

db.clientes.on('removed', (removedCliente) => {
  EventBus.$emit('DELETE_CLIENTE_SUCCESS', removedCliente)
})

db.receitas.on('inserted', (receita) => {
  db.receitas.find({}, (error, receitas) => EventBus.$emit('INSERT_RECEITA_SUCCESS', receita, receitas))
})

db.receitas.on('updated', (newReceita, oldReceita) => {
  EventBus.$emit('UPDATE_RECEITA_SUCCESS', newReceita, oldReceita)
})

db.receitas.on('removed', (removedReceita) => {
  EventBus.$emit('DELETE_RECEITA_SUCCESS', removedReceita)
})

export default db