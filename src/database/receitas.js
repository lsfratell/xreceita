import db from './connection'
import EventBus from '../eventBus'

class Receitas {

  constructor() {
    this.connect()
  }

  connect() {
    EventBus.$on('GET_RECEITA', this.getOne)
    EventBus.$on('INSERT_RECEITA', this.insert)
    EventBus.$on('UPDATE_RECEITA', this.update)
    EventBus.$on('DELETE_RECEITA', this.delete)
    EventBus.$on('ALL_RECEITAS', this.getAll)
    EventBus.$on('COUNT_RECEITAS', this.count)
  }

  count() {
    db.receitas.count({}, (error, count) => {
      EventBus.$emit('COUNT_RECEITAS_SUCCESS', count)
    })
  }

  getOne(id) {
    db.receitas.findOne({ _id: id }, (error, receita) => {
      db.clientes.findOne({ _id: receita.cliente_id }, (error2, cliente) => {
        EventBus.$emit('GET_RECEITA_SUCCESS', error, { ...receita, cliente: cliente })
      })
    })
  }

  getAll(id) {
    db.receitas.find({ cliente_id: id }).sort({ createdAt: -1 }).exec((error, receitas) => {
      db.clientes.findOne({ _id: id }, (error2, cliente) => {
        EventBus.$emit('ALL_RECEITAS_SUCCESS', error, { ...cliente, receitas: receitas })
      })
    })
  }

  insert(payload) {
    db.receitas.insert(payload)
  }

  update(id, form) {
    db.receitas.update({ _id: id }, { $set: { ...form }}, {})
  }

  delete(id) {
    db.receitas.remove({ _id: id }, {})
  }
}

const receitas = new Receitas()

export default receitas