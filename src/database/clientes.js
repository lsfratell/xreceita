import db from './connection'
import EventBus from '../eventBus'

class Clientes {

  constructor() {
    this.connect()
  }

  connect() {
    EventBus.$on('GET_CLIENTE', this.getOne)
    EventBus.$on('INSERT_CLIENTE', this.insert)
    EventBus.$on('UPDATE_CLIENTE', this.update)
    EventBus.$on('DELETE_CLIENTE', this.delete)
    EventBus.$on('ALL_CLIENTES', this.getAll)
    EventBus.$on('COUNT_CLIENTES', this.count)
  }

  count() {
    db.clientes.count({}, (error, count) => {
      EventBus.$emit('COUNT_CLIENTES_SUCCESS', count)
    })
  }

  getOne(id) {
    db.clientes.findOne({ _id: id }, (error, cliente) => {
      db.receitas.find({ 'cliente_id': cliente._id }, (error2, receitas) => {
        EventBus.$emit('GET_CLIENTE_SUCCESS', error, { ...cliente, receitas: receitas })
      })
    })
  }

  getAll() {
    db.clientes.find({}).sort({ createdAt: -1 }).exec((error, clientes) => {
      EventBus.$emit('ALL_CLIENTES_SUCCESS', error, clientes)
    })
  }

  insert(form) {
    db.clientes.insert(form)
  }

  update(id, form) {
    db.clientes.update({ _id: id }, { $set: { ...form }}, {})
  }

  delete(id) {
    db.clientes.remove({ _id: id }, {}, () => {
      db.receitas.remove({ cliente_id: id }, { multi: true })
    })
  }
}

const clientes = new Clientes()

export default clientes