import clientes from './database/clientes'
import receitas from './database/receitas'

import Vue from 'vue'
import Buetify from 'buefy'
import 'buefy/dist/buefy.css'
import '@mdi/font/css/materialdesignicons.css'

import App from './App'
import router from './router'

Vue.use(Buetify, {
  defaultIconPack: 'mdi'
})

import { VueMaskDirective } from 'v-mask'
Vue.directive('mask', VueMaskDirective)

Vue.prototype.$remote = window.require('electron').remote
Vue.prototype.$jdenticon = window.require('jdenticon')

Vue.directive('uppercase', {
  bind(el, _, vnode) {
    el.childNodes[0].addEventListener('keyup', (e) => {
      vnode.componentInstance.$emit('input', e.target.value.toUpperCase())
      e.target.value = e.target.value.toUpperCase()
    })
  }
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})