import Vue from 'vue'
import VueRouter from 'vue-router'
import Clientes from './pages/Clientes'
import Receitas from './pages/Receitas'
import Receita from './reports/Receita'

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { path: '/', name: 'clientes', component: Clientes },
    { path: '/:id/receitas', name: 'receitas', component: Receitas },
    { path: '/:id/receitas/:rid', name: 'receita', component: Receita },
  ]
})

export default router