// Modules to control application life and create native browser window
const { app, BrowserWindow, globalShortcut } = require('electron')
const path = require('path')
const isDev = require('electron-is-dev')

function createWindow () {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1152,
    height: 648,
    frame: false,
    show: false,
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, 'preload.js'),
    }
  })

  mainWindow.once('ready-to-show', () => mainWindow.show())

  mainWindow.webContents.on('new-window', (event, url, frameName, disposition, options) => {
    event.preventDefault()

    const receitaWindow = new BrowserWindow({
      width: 660,
      height: 690,
      frame: false,
      show: false,
      resizable: false,
      webPreferences: {
        nodeIntegration: true
      }
    })

    receitaWindow.webContents = options.webContents

    receitaWindow.on('ready-to-show', () => receitaWindow.show())
    receitaWindow.on('show', () => mainWindow.hide())
    receitaWindow.on('close', () => mainWindow.show())

    if (!options.webContents)
      receitaWindow.loadURL(url)

    event.newGuest = receitaWindow
  })

  if (isDev)
    mainWindow.loadURL('http://localhost:1234')
  else 
    mainWindow.loadFile('build/index.html')
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  globalShortcut.register('CommandOrControl+Shift+F12', () => {
    BrowserWindow.getFocusedWindow().webContents.openDevTools()
  })

  globalShortcut.register('CommandOrControl+Shift+F5', () => {
    BrowserWindow.getFocusedWindow().reload()
  })

  createWindow()
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
app.on('will-quit', () => {
  globalShortcut.unregisterAll()
})